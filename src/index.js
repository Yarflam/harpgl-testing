import { MapView } from '@here/harp-mapview';
import { GeoCoordinates } from '@here/harp-geoutils';
import { MapControls } from '@here/harp-map-controls';
import {
    APIFormat,
    AuthenticationTypeMapboxV4,
    OmvDataSource
} from '@here/harp-omv-datasource';
import Noty from 'noty';

/* Configuration */
import privateConfig from '../config/private.config.json';

/* Styles */
import '../node_modules/noty/lib/noty.css';
import '../node_modules/noty/lib/themes/mint.css';
import './assets/index.scss';

/* Get the token */
const token = ((privateConfig || {}).HERE || {}).token;

/* Check */
new Noty({
    type: token ? 'success' : 'error',
    layout: 'topRight',
    text: 'Token: ' + (token ? 'FOUND' : 'NOT FOUND')
}).show();

/* Canvas */
const mapCanvas = document.querySelector('canvas');

/* MapView */
const mapView = new MapView({
    canvas: mapCanvas,
    theme: 'resources/berlin_base.json',
    decoderUrl: 'js/harp-gl-decoders.bundle.js'
});

/* Camera position */
mapView.camera.position.set(0, 0, 800);
mapView.geoCenter = new GeoCoordinates(40.70398928, -74.01319808, 0);
mapView.resize(mapCanvas.clientWidth, mapCanvas.clientHeight);

/* Data source */
if (token) {
    const dataSource = new OmvDataSource({
        baseUrl: 'https://xyz.api.here.com/tiles/herebase.02',
        apiFormat: APIFormat.XYZOMV,
        styleSetName: 'tilezen',
        maxZoomLevel: 17,
        authenticationCode: privateConfig.HERE.token,
        authenticationMethod: AuthenticationTypeMapboxV4
    });
    mapView.addDataSource(dataSource);
}

/* Map Controls */
MapControls.create(mapView);
