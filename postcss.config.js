const postcssPresetEnv = require('postcss-preset-env');

module.exports = {
    plugins: [
        require('autoprefixer'),
        require('postcss-calc'),
        require('postcss-flexbugs-fixes'),
        require('postcss-for'),
        require('postcss-import'),
        require('postcss-mixins'),
        require('postcss-nesting'),
        require('postcss-percentage'),
        require('postcss-simple-vars'),
        postcssPresetEnv({
            stage: 0,
            features: {
                'nesting-rules': true,
                'color-mod-function': true,
                'custom-media': true
            }
        })
    ]
};
