# HarpGl Testing

![license](https://img.shields.io/badge/license-Apache_2.0-green.svg)
![version](https://img.shields.io/badge/app-vTest-blue.svg)

Create 3D map using harp.gl module. [See more](https://github.com/heremaps/harp.gl)

- @here/harp-map-controls
- @here/harp-map-theme
- @here/harp-mapview
- @here/harp-omv-datasource

## Install

You can clone the project:

> $> git clone https://gitlab.com/Yarflam/harpgl-testing
>
> $> npm install

## Configure

### Account

1. Create an account on [HERE](https://developer.here.com/).
2. Generate the API keys (JAVASCRIPT/REST section on dashboard);
3. Connect on [Token Manager](https://xyz.api.here.com/token-ui/) with the API keys.
4. Select your configuration (READ_DATA, WRITE_DATA, ...), click on 'Generate Token', copy it.
5. Edit the config file `config/private.config.json.example` : remove `.example` in the filename and paste your token.

### Server

The server listen on port `3040`. You can change it in `config/public.config.json`.

## Starting

You can start the web server and webpack as like:

> $> npm start

And go on [http://127.0.0.1:3040/](http://127.0.0.1:3040/).

![App Screenshot](public/images/screen.jpg)

## Structure

- **/config**: webpack configuration and global variables (token, port).
- **/public**: every files expose on the web server.
- **/src**: project's source
    - **/src/assets**: css/sass files

## Dependencies

- HERE - Harp map
- Babel v7
- PostCSS preset v6
- Eslint v5
- Three.js v0.99
- Webpack v4
- Noty v3

## Authors

- Yarflam - *initial work*

## License

The project is licensed under Apache 2.0 - see the [LICENSE](LICENSE) file for details.
