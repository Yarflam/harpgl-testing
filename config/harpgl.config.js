const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

const webpackSets = require('./webpack.sets');

module.exports = {
    target: 'webworker',
    mode: process.env.NODE_ENV || 'development',
    entry: {
        decoder: './src/harp-gl-decoders.js'
    },
    output: {
        path: webpackSets.public_path,
        filename: 'harp-gl-decoders.bundle.js'
    },
    module: {
        rules: [webpackSets.jsx, webpackSets.scss]
    },
    plugins: [
        new CopyPlugin([
            {
                from: path.resolve(
                    __dirname,
                    '..',
                    'node_modules',
                    '@here',
                    'harp-map-theme',
                    'resources'
                ),
                to: path.resolve(__dirname, '..', 'public', 'resources')
            }
        ])
    ]
};
