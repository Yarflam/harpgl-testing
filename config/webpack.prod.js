const webpackSets = require('./webpack.sets');

/* Harp.gl */
const harpglConfig = require('./harpgl.config');
process.env.NODE_ENV = 'production';

module.exports = [
    {
        mode: 'production',
        entry: './src/index.js',
        output: {
            path: webpackSets.public_path,
            filename: 'app.bundle.js'
        },
        module: {
            rules: [webpackSets.jsx, webpackSets.scss, ...webpackSets.fonts]
        }
    },
    harpglConfig
];
